import Vue from 'vue'
import Vuex from 'vuex'
import * as axios from "axios";

Vue.use(Vuex)

const backendURL = 'http://stackoverflow.udachin.tech'

export default new Vuex.Store({
  state: {
    user: {
      authorized: false,
      id: null,
      nickname: null,
      permission: null,
    },
    showNav: false,
  },
  mutations: {
    toggleNav(state) {
      state.showNav = !state.showNav
    },
    resetUser(state) {
      state.user = {
        authorized: false,
        id: null,
        nickname: null,
      }
    },
    setUser(state, {nickname, id, permission}) {
      state.user = {
        authorized: true,
        nickname,
        id,
        permission,
      }
    }
  },
  actions: {
    async registerUser({}, {nickname, password, permission = 'user'}) {
      if (nickname && password && ['user', 'admin', 'manager'].includes(permission)) {
        const body = {nickname, password, permission}
        const { data } = await axios.post(`${backendURL}/user/create`, body)
        return data.data
      }
    },
    async authUser({}, {nickname, password}) {
      if (nickname && password) {
        const body = {nickname, password}
        const { data } = await axios.post(`${backendURL}/user/auth`, body)
        return data.data
      }
    },
    async getQuestions({}, {userId, segmentId, search, isPublished = true}) {
      let query = `?isPublished=${isPublished}`
      if (userId) query = query + `&userId=${userId}`
      if (segmentId) query = query + `&segmentId=${segmentId}`
      if (search) query = query + `&search=${search}`
      const { data } = await axios.get(`${backendURL}/question${query}`)
      return data.data
    },
    async getQuestionById({}, {id}) {
      const { data } = await axios.get(`${backendURL}/question/${id}`)
      return data.data
    },
    async likeQuestion({}, {id}) {
      const { data } = await axios.put(`${backendURL}/question/mark`, {id})
      return data
    },
    async createQuestion({state}, body) {
      const { data } = await axios.post(`${backendURL}/question/create`, {
        ...body,
        userId: state.user.id
      })
      return data
    },
    async deleteQuestion({}, {id}){
      const { data } = await axios.delete(`${backendURL}/question/${id}`)
      return data
    },
    async acceptQuestion({}, {id}){
      const { data } = await axios.put(`${backendURL}/question/publish`, {id})
      return data
    },
    async getCategories() {
      const { data } = await axios.get(`${backendURL}/segment`)
      return data.data
    },
    async getComments({}, {id}) {
      const { data } = await axios.get(`${backendURL}/comment/${id}`)
      return data.data
    },
    async createCategory({}, body) {
      const { data } = await axios.post(`${backendURL}/segment/create`, body)
      return data
    },
    async changeCategoryName({}, {id, name}) {
      const { data } = await axios.put(`${backendURL}/segment`, {id, name})
      return data
    },
    async postComment({state}, {questionId, text}) {
      const { data } = await axios.post(`${backendURL}/comment/create`, {
        text,
        questionId,
        userId: state.user.id,
      })
      return data.data
    },
    async getUsers({state}, {search}) {
      const {data} = await axios.get(`${backendURL}/user${search ? '?search=' + search : ''}`)
      return data.data
    },
    async deleteUser({}, {id}) {
      const {data} = await axios.delete(`${backendURL}/user/${id}`)
      return data
    },
    async changeUserRole({}, body) {
      const {data} = await axios.put(`${backendURL}/user/permission`, body)
      return data
    }
  },
  modules: {
  }
})
